// def/undef this depending on if we want measurements or not. This has to happen
// *before* including "measurement_tool.h".
#define MEASURE
#include "../measurement_tool.h"

#include "test.h"

#include <cstdlib>

int main()
{
	for (int i = 0; i < 100; ++i) {
		MEASUREMENT::start(EXP::INNER_LOOP_ALL);
		for (int j = 0; j < 1000; ++j) {
			auto sample = rand()%100;
			MEASUREMENT::addInt(EXP::SAMPLE_VALUES, sample);

			if(rand()%100 < 10) {
				MEASUREMENT::inc(EXP::IF_COUNTER);
			}
		}
		MEASUREMENT::stop(EXP::INNER_LOOP_ALL);

		MEASUREMENT::addInt(EXP::SAMPLE_VALUE, 23);
		MEASUREMENT::reset(EXP::SAMPLE_VALUE);
		MEASUREMENT::addInt(EXP::SAMPLE_VALUE, 42);
	}

	// MEASUREMENT::print(EXP::INNER_LOOP);
	// MEASUREMENT::print(EXP::INNER_LOOP_ALL);
	// MEASUREMENT::print(EXP::IF_COUNTER);
	// MEASUREMENT::print(EXP::SAMPLE_VALUE);
	// MEASUREMENT::print(EXP::SAMPLE_VALUES);
	MEASUREMENT::print();
}
